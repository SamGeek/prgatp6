package dagba.leduc.tp6;

import java.util.Iterator;

import dagba.leduc.tp6.modele.Case;


public class IterateurMots implements Iterator<String> {
	
	Object[] tab;
	int cursor=0;
	Case current = new Case();
	String nextElt="";

	public IterateurMots(Object[] tab) {
		this.tab = tab;
		//mettre le curseur sur le premier element non nul du tableau
		while(cursor<tab.length && ((Case)tab[cursor]).isCaseNoire()) {
			cursor+=1;
		}
	}
	
	//il n'y a pas de methode remove que lon doit redefinir apres reimplementation 
	// de l'interface iterator

	@Override
	public boolean hasNext() {
		return cursor<tab.length ;
	}

	@Override
	public String next() {
		//cette methode retourne la chaine de caracteres avant le prochain element null
		
		if(cursor < tab.length) current = (Case) tab[cursor];
		
		while(cursor < tab.length && !current.isCaseNoire()) {
			//System.out.println("solution = "+current.getSolution());
			nextElt+=current.getSolution();
			cursor+=1;
			if(cursor < tab.length) current = (Case) tab[cursor];
		}
		
		if(current.isCaseNoire()) nextElt+="*";
		
		while(cursor<tab.length && current.isCaseNoire()) {	
			cursor+=1;
			current = (Case) tab[cursor];
		}
		
		
		
		return nextElt;
	}

}
