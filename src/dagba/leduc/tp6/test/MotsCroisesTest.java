package dagba.leduc.tp6.test;


import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import dagba.leduc.tp6.ChargerGrille;
import dagba.leduc.tp6.MotsCroisesTP6;
import dagba.leduc.tp6.modele.MotsCroisesFactory;

class MotsCroisesTest {

	private int numGrille;
	private String controle ;

	@Before
	public void setUp() throws Exception {
		this.numGrille = 10;
		controle = "";
	}

	@Test
	void test() {

		// recuperer une grille depuis la base et verifier le champ controle
		ChargerGrille cg = new ChargerGrille();
		try {

//			Connection maConnection = DriverManager.getConnection("jdbc:mysql://192.168.64.2:3306/tp3_prga", "samuel","samuel");
			
			Connection maConnection = DriverManager.getConnection("jdbc:mysql://mysql.istic.univ-rennes1.fr:3306/base_bousse", "user_18003861", "190100Ff&&");

			String req2 = "SELECT controle FROM TP5_GRILLE WHERE num_grille = ?";
			PreparedStatement pstmt = maConnection.prepareStatement(req2);
			pstmt.setInt(1, numGrille);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				controle = rs.getString("controle");
			}

			
			MotsCroisesTP6 jeu = cg.extraireGrille(numGrille);

			assertEquals(controle.toUpperCase(), jeu.solutions().toUpperCase());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testMotsCroisesFactory()
	{
		
//		mc.setCaseNoire(2, 2, true);
//		mc.setDefinition(1, 1, true, "Note");
//		mc.setSolution(1, 1, 'S');
//		mc.setSolution(1, 2, 'O');
//		mc.setSolution(1, 3, 'L');
//		mc.setDefinition(1, 1, false, "Autre note");
//		mc.setSolution(2, 1, 'I');
//		mc.setDefinition(1, 3, false, "Et encore une note");
//		mc.setSolution(2, 3, 'A');
		
		MotsCroisesTP6 mc = MotsCroisesFactory.creerMotsCroises2x3();
		assertTrue(mc.estCaseNoire(2, 2));
		assertEquals("Note", mc.getDefinition(1, 1, true));
		assertEquals("Autre note", mc.getDefinition(1, 1, false));
		assertEquals("Et encore une note", mc.getDefinition(1, 3, false));
	}

}
