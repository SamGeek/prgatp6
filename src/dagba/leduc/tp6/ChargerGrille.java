package dagba.leduc.tp6;

import java.sql.*;
import java.util.*;

public class ChargerGrille {
	static Map<Integer, String> grilles;
	private static Connection maConnection;

	public ChargerGrille() {
		try {
			maConnection = connecterBD();
			grilles = new HashMap<>();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Connection connecterBD() throws SQLException {
		Connection connect;
//		connect = DriverManager.getConnection("jdbc:mysql://mysql.istic.univ-rennes1.fr:3306/base_bousse", "user_18003861", "190100Ff&&");
		connect = DriverManager.getConnection("jdbc:mysql://192.168.64.2:3306/tp3_prga", "samuel", "samuel");
		return connect;
	}

	public static Map<Integer, String> grillesDisponibles() throws SQLException {
		String req1 = "SELECT num_grille, nom_grille, largeur, hauteur FROM TP5_GRILLE";
		PreparedStatement pstmt = maConnection.prepareStatement(req1);
		ResultSet rs = pstmt.executeQuery(req1);
		String desc = "";
		while (rs.next()) {
			int numero = rs.getInt("num_grille");
			String nom = rs.getString("nom_grille");
			int largeur = rs.getInt("largeur");
			int hauteur = rs.getInt("hauteur");
			desc = nom + " (" + hauteur + "x" + largeur + ")";
			grilles.put(numero, desc);
		}
		return grilles;
	}

	public MotsCroisesTP6 extraireGrille(int numGrille) throws SQLException {

		String req2 = "SELECT hauteur,largeur, controle FROM TP5_GRILLE WHERE num_grille = ?";
		PreparedStatement pstmt = maConnection.prepareStatement(req2);
		pstmt.setInt(1, numGrille);
		ResultSet rs = pstmt.executeQuery();
		int haut = 0;
		int larg = 0;
		while (rs.next()) {
			haut = rs.getInt("hauteur");
			larg = rs.getInt("largeur");
		}
		

		MotsCroisesTP6 jeu = new MotsCroisesTP6(haut, larg);
		
		
		String req3 = "SELECT * FROM TP5_MOT WHERE num_grille = ?";
		
		PreparedStatement pstmt2 = maConnection.prepareStatement(req3);
		pstmt2.setInt(1, numGrille);
		
		
		ResultSet rs2 = pstmt2.executeQuery();
		String definition, solution;
		int horizontal, ligne, colonne;
		while (rs2.next()) {
			definition = rs2.getString("definition");
			horizontal = rs2.getInt("horizontal");
			ligne = rs2.getInt("ligne");
			colonne = rs2.getInt("colonne");
			solution = rs2.getString("solution");
			

			int compteur = 0;
			
			if(horizontal==1) {
				jeu.setDefinition(ligne, colonne, true, definition);
				for (int j = colonne; j<=colonne+solution.length()-1;j++) {
					jeu.setCaseNoire(ligne, j, false);
					jeu.setSolution(ligne, j, solution.charAt(compteur++));
				}
			}else {
				jeu.setDefinition(ligne, colonne, false, definition);
				for (int i = ligne; i<=ligne+solution.length()-1;i++) {
					jeu.setCaseNoire(i, colonne, false);
					jeu.setSolution(i, colonne, solution.charAt(compteur++));
				}
			}
		}

		return jeu;
	}
}
