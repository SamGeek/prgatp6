package dagba.leduc.tp6.modele;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Case{

	// Variables d'instance
	private Character solution;
	private StringProperty proposition;
	private String horizontal, vertical;
	private boolean caseNoire;
	
	public Case() {
		solution = ' ';
		proposition = new SimpleStringProperty("");
		horizontal="";
		vertical="";
		caseNoire = true;
		
	}

	

	public Character getSolution() {
		return solution;
	}

	public void setSolution(Character solution) {
		this.solution = solution;
	}

	public StringProperty getProposition() {
		return proposition;
	}

	public void setProposition(String proposition) {
		this.proposition.set(proposition);
	}

	public String getHorizontal() {
		return horizontal;
	}

	public void setHorizontal(String horizontal) {
		this.horizontal = horizontal;
	}

	public String getVertical() {
		return vertical;
	}

	public void setVertical(String vertical) {
		this.vertical = vertical;
	}

	public boolean isCaseNoire() {
		return caseNoire;
	}

	public void setCaseNoire(boolean caseNoire) {
		this.caseNoire = caseNoire;
	}
	
	
	@Override
	public String toString() {
		return "Case : solution = " + solution + "\n" + " proposition = " + proposition + "\n" + " horizontal = "
				+ horizontal + "\n" + " vertical = " + vertical + "\n"
				+ " est Case Noire = " + caseNoire + "\n";
	}

}
