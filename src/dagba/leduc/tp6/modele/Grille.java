package dagba.leduc.tp6.modele;

import dagba.leduc.tp6.IterateurMots;

public class Grille<E> {
	// Variables d’instance
	int hauteur ;//nombre de lignes 
	int largeur ;// nombre de colonnes
	E[][] tab;//tableau de chaînes de caractères à deux dimensions,
	// avec taille = hauteur x largeur
	// Constructeur permettant d’obtenir une grille
	// dotée d’un tableau de dimensions conformes aux valeurs
	// respectives de hauteur et de largeur, dont tous les
	// éléments contiennent la valeur null.
	// Précondition (assert) : hauteur   0 et largeur   0 ≥ ≥
	public Grille (int hauteur, int largeur) {
		if(hauteur>=0 && largeur>=0) {
			this.hauteur = hauteur;
			this.largeur = largeur;
			tab = (E[][]) new Object[hauteur][largeur];
		}
		
	}
	// Accesseurs (getters)
	public int getHauteur() {
		return hauteur;
	}
	public int getLargeur() {
		return largeur;
		
	}
	// Validité des coordonnées
	// Resultat : vrai si et seulement si lig (resp. col)
	// est compris entre 1 et getHauteur() (resp. getlargeur())
	public boolean coordCorrectes(int lig, int col) {
		return (lig>=1&&lig<=getHauteur() && col>=1&&col<=getLargeur());
	}
	// Valeur de la cellule ayant pour coordonnées (lig, col)
	// Précondition (assert) : coordCorrectes(lig, col)
	public E getCellule(int lig, int col) {
		
		assert coordCorrectes(lig, col) : "coordonnées incorrectes";
		E retour ;

		lig = lig-1;col =col-1;
		retour=  tab[lig][col];
		return retour;
	}
	// Modification de la cellule de coordonnées (lig, col)
	// Précondition (assert) : coordCorrectes(lig, col)
	public void setCellule(int lig, int col, E ch) {
		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		lig = lig-1;col =col-1;
		tab[lig][col]=ch;
	}
	
	
	public IterateurMots iterateurMots(boolean horizontal, int num) {
		IterateurMots it;
		Object[] array;
		
		if(horizontal) {
			array =new Object[largeur];
		
			for (int i=1;i<largeur;i++) {
				array[i-1] = this.tab[num][i];
			}
			
			it = new  IterateurMots(array);
		}else {
			array =new Object[hauteur];
			
			for (int i=1;i<=hauteur;i++) {
				array[i-1] = this.tab[i][num];
			}
			
			it = new  IterateurMots(array);
			
		}
		 
		return it;
	}
	
	// Texte sur “hauteur” lignes, colonnes séparées par des |
	// (voir exemple plus loin)
	@Override
	public String toString()  {
		String output="";
		for(int i=0;i<hauteur;i++) {
			for(int j=0;j<largeur;j++) {
				output+=tab[i][j];
				if(j!=largeur-1) output+= "|";
			}
			 output+= "\n";
		}
		return output;
	}
	}