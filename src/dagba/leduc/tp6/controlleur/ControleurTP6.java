package dagba.leduc.tp6.controlleur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dagba.leduc.tp6.MotsCroisesTP6;
import dagba.leduc.tp6.modele.Case;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

public class ControleurTP6 {

	@FXML // pr rendre la m�thode visible depuis SceneBuilder
	private GridPane monGridPane;

	@FXML
	private ProgressBar progressionJeu;

	private MotsCroisesTP6 mc;
	private static Connection maConnection;

	private boolean isRandom, checkAnswers;
	private int numGrille, nombrePropositions = 0;

	private TextField caseCourante;
	
	private boolean directionIsHorizontal = true, isFirst ;
	private int currentLig=0, currentCol = 0;
	
	@FXML
	private TextField[][] textFields;
	
	
	private String defaultTextFieldStyle;
	private final String succesTextFieldStyle = "-fx-control-inner-background: #81C784";

	

	public ControleurTP6(boolean isRandom, int numGrilleToLoad) {
		this.isRandom = isRandom;
		this.numGrille = numGrilleToLoad;
		isFirst =false;
		// System.out.println("random ? "+ isRandom+ " numero de grille ? "+numGrille);
	}

	@FXML
	public void clicCase(MouseEvent e) {
		
		Node n = (Node) e.getSource();
		int lig = ((int) n.getProperties().get("gridpane-row")) + 1;
		int col = ((int) n.getProperties().get("gridpane-column")) + 1;
		System.out.println("middle click");
		currentLig =  lig-1;
		currentCol = col-1;
		
		
		// changer later with mouse middle
		if (e.getButton() == MouseButton.MIDDLE) {
			mc.reveler(lig, col);
		}
	}

	public static Connection connecterBD() throws SQLException {
		Connection connect;
		connect = DriverManager.getConnection("jdbc:mysql://mysql.istic.univ-rennes1.fr:3306/base_bousse", "user_18003861", "190100Ff&&");
//		connect = DriverManager.getConnection("jdbc:mysql://192.168.64.2:3306/tp3_prga", "samuel", "samuel");
		return connect;
	}

	@FXML
	private void initialize() {
		// deuxieme option, charger une grille depuis la base de données
		if (isRandom == true) {
			// load a Random Game between available ones
			List<Integer> keys = new ArrayList<Integer>(ControleurAcceuil.grilles.keySet());
			Collections.shuffle(keys);
			extractFromDatabase(Integer.parseInt(keys.get(0).toString()));
		} else {
			System.out.println("Extraction de la grille numero " + numGrille);
			extractFromDatabase(numGrille);
		}

		for (Node n : monGridPane.getChildren()) {
			if (n instanceof TextField) {
				TextField tf = (TextField) n;
				int lig = ((int) n.getProperties().get("gridpane-row")) + 1;
				int col = ((int) n.getProperties().get("gridpane-column")) + 1;

				tf.textProperty().bindBidirectional(mc.propositionProperty(lig, col));
				addTextLimiter(tf, 1);
				
				
				textFields[lig-1][col-1] =  tf;
			
				tf.setOnMouseClicked((e) -> {
					caseCourante = tf;
					this.clicCase(e);
				});

				tf.setOnKeyReleased((e) -> {
					updateProgression();
					changeDirection(e);
				});

				// recuperation du tooltip a afficher dans la case
				String defHori = "", defVerti = "", def = "";
				try {
					defHori = mc.getDefinition(lig, col, true);
					defVerti = mc.getDefinition(lig, col, false);
					def = "";
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (defHori != null && !defHori.equals(""))
					def += defHori;
				if (defVerti != null && !defVerti.equals("") && !def.equals(""))
					def += " / " + defVerti;
				else if (defVerti != null && !defVerti.equals("") && def.equals("")) {
					def += defVerti;
				}

				if (defHori != null && !defHori.equals("") || defVerti != null && !defVerti.equals(""))
					tf.setTooltip(new Tooltip(def));
			}
		}
	}

	private void changeDirection(KeyEvent e) {
		TextField n = (TextField) e.getSource();
		int lig = ((int) n.getProperties().get("gridpane-row"));
		int col = ((int) n.getProperties().get("gridpane-column"));
		
		
		System.out.println("Les indices au debut sont : " +currentLig+" "+ currentCol);
		
		Node noeud = null;
		
		KeyCode c =  e.getCode();
		
		switch (c) {
			case BACK_SPACE:
				//n.setStyle(defaultTextFieldStyle);

				if(directionIsHorizontal==true && col > 0) {
						currentCol = col-1;				
				}else if(directionIsHorizontal==false && lig > 0) {
						currentLig =  lig-1;
				}
				
				noeud = textFields[currentLig][currentCol];
				if (noeud != null) {
					caseCourante = (TextField) noeud;
					caseCourante.requestFocus();
				}
				return;
			case RIGHT:
				if (col < mc.getLargeur()) {
					currentCol = col+1;	
					directionIsHorizontal = true;
					
					noeud = textFields[currentLig][currentCol];
					if (noeud != null) {
						caseCourante = (TextField) noeud;
						caseCourante.requestFocus();
					}
				}
					
				break;
			case LEFT:
				if (col > 0) {
					currentCol = col-1;
					directionIsHorizontal = true;
					
					noeud = textFields[currentLig][currentCol];
					if (noeud != null) {
						caseCourante = (TextField) noeud;
						caseCourante.requestFocus();
					}
				}
				break;
			case DOWN:
				System.out.println("on est dans la touche bas et la ligne vaut "+ lig);
				if (lig < mc.getHauteur()) {
					System.out.println("on rentre bien dans le if");
					currentLig = lig+1;
					directionIsHorizontal = false;
					
					noeud = textFields[currentLig][currentCol];
					if (noeud != null) {
						System.out.println("le noeud courrant n'est pas null");
						caseCourante = (TextField) noeud;
						caseCourante.requestFocus();
					}
				}
				break;
			case UP:
				if (lig > 0) {
					currentLig = lig-1;
					directionIsHorizontal = false;
					noeud = textFields[currentLig][currentCol];
					if (noeud != null) {
						caseCourante = (TextField) noeud;
						caseCourante.requestFocus();
					}
					
				}
				break;
				
			case ENTER:
				checkAnswers = true;
				
				if (mc.getProposition(lig + 1, col + 1) != mc.getProposition(lig + 1, col + 1)) {
					n.setStyle(defaultTextFieldStyle);
				}
				
				// colorier en vert le background de toutes les cases justes
				for (int i = 0; i < mc.getHauteur(); i++) {
					for (int j = 0; j < mc.getLargeur(); j++) {
						
						if (!mc.jeu.getCellule(i + 1, j + 1).isCaseNoire()
								&& mc.getProposition(i + 1, j + 1) == mc.getSolution(i + 1, j + 1)) {
							noeud = textFields[i][j];
							noeud.setStyle(succesTextFieldStyle);
						}
					}
				}
				return;
				
			default:
				
				if(n.getText().isEmpty()) return;
				
				System.out.println("on est la");
				if(c!=KeyCode.RIGHT && c!=KeyCode.LEFT && c!=KeyCode.UP && c!=KeyCode.DOWN) {
					if(directionIsHorizontal) {
						if (col < mc.getLargeur()-1) {
							System.out.println("toto");
							currentCol = col+1;	
							noeud = textFields[currentLig][currentCol];
							if (noeud != null) {
								caseCourante = (TextField) noeud;
								caseCourante.requestFocus();
							}
						}
					}else {
						if (lig < mc.getHauteur()-1) {
							currentLig = lig+1;	
							noeud = textFields[currentLig][currentCol];
							if (noeud != null) {
								caseCourante = (TextField) noeud;
								caseCourante.requestFocus();
							}
						}
						}
					}
				
				break;

		}	
		System.out.println("Les indices actuels sont : " +currentLig+" "+ currentCol);
		
		

	}

	private void updateProgression() {
		int compteur = 0;

		for (int i = 0; i < mc.jeu.getHauteur(); i++) {
			for (int j = 0; j < mc.jeu.getLargeur(); j++) {
				if (mc.getProposition(i + 1, j + 1) != ' ')
					compteur++;
			}
		}

		double progressValue = (double) compteur / (double) nombrePropositions;

		progressionJeu.setProgress(progressValue);

	}

	public void extractFromDatabase(int numGrille) {
		try {
			maConnection = connecterBD();
			mc = extraireGrille(numGrille);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setFields();

	}

	private void setFields() {
		// TODO Auto-generated method stub
		TextField modele = (TextField) monGridPane.getChildren().get(0);
		defaultTextFieldStyle = modele.getStyle();
		monGridPane.getChildren().clear();
		int hauteur = mc.getHauteur(), largeur = mc.getLargeur();
		System.out.println("hauteur : " + hauteur);

		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				Case c = mc.jeu.getCellule(j + 1, i + 1);
				if (c.isCaseNoire())
					continue;
				
				
				
				nombrePropositions++;
				TextField tfc = new TextField();
				tfc.setPrefWidth(modele.getPrefWidth());
				tfc.setPrefHeight(modele.getPrefHeight());
				for (Object cle : modele.getProperties().keySet()) {
					tfc.getProperties().put(cle, modele.getProperties().get(cle));
				}
				monGridPane.add(tfc, i, j);
				
				if(!isFirst) {
					isFirst= true;
					System.out.println("Les indices de la premiere case sont : " +i+" "+ j);
					currentLig = i;
					currentCol = j;
					tfc.requestFocus();
				}
			}
		}

	}
	
	

	public MotsCroisesTP6 extraireGrille(int numGrille) throws SQLException {

		// a refaire
		// TODO: faire une jointure des deux requetes pour simplifier
		String req2 = "SELECT hauteur,largeur, controle FROM TP5_GRILLE WHERE num_grille = ?";
		PreparedStatement pstmt = maConnection.prepareStatement(req2);
		pstmt.setInt(1, numGrille);
		ResultSet rs = pstmt.executeQuery();
		int haut = 0;
		int larg = 0;
		while (rs.next()) {
			haut = rs.getInt("hauteur");
			larg = rs.getInt("largeur");
		}

		MotsCroisesTP6 jeu = new MotsCroisesTP6(haut, larg);
		textFields = new TextField[haut][larg];

		String req3 = "SELECT * FROM TP5_MOT WHERE num_grille = ?";

		PreparedStatement pstmt2 = maConnection.prepareStatement(req3);
		pstmt2.setInt(1, numGrille);

		ResultSet rs2 = pstmt2.executeQuery();
		String definition, solution;
		int horizontal, ligne, colonne;
		while (rs2.next()) {
			definition = rs2.getString("definition");
			horizontal = rs2.getInt("horizontal");
			ligne = rs2.getInt("ligne");
			colonne = rs2.getInt("colonne");
			solution = rs2.getString("solution");

			int compteur = 0;

			if (horizontal == 1) {
				jeu.setDefinition(ligne, colonne, true, definition);
				for (int j = colonne; j <= colonne + solution.length() - 1; j++) {
					jeu.setCaseNoire(ligne, j, false);
					jeu.setSolution(ligne, j, solution.charAt(compteur++));
				}
			} else {
				jeu.setDefinition(ligne, colonne, false, definition);
				for (int i = ligne; i <= ligne + solution.length() - 1; i++) {
					jeu.setCaseNoire(i, colonne, false);
					jeu.setSolution(i, colonne, solution.charAt(compteur++));
				}
			}
		}

		return jeu;
	}
	

	public MotsCroisesTP6 extraireGrilleWithConnection(int numGrille, Connection maConnection) throws SQLException {

		// a refaire
		// TODO: faire une jointure des deux requetes pour simplifier
		String req2 = "SELECT hauteur,largeur, controle FROM TP5_GRILLE WHERE num_grille = ?";
		PreparedStatement pstmt = maConnection.prepareStatement(req2);
		pstmt.setInt(1, numGrille);
		ResultSet rs = pstmt.executeQuery();
		int haut = 0;
		int larg = 0;
		while (rs.next()) {
			haut = rs.getInt("hauteur");
			larg = rs.getInt("largeur");
		}

		MotsCroisesTP6 jeu = new MotsCroisesTP6(haut, larg);
		textFields = new TextField[haut][larg];

		String req3 = "SELECT * FROM TP5_MOT WHERE num_grille = ?";

		PreparedStatement pstmt2 = maConnection.prepareStatement(req3);
		pstmt2.setInt(1, numGrille);

		ResultSet rs2 = pstmt2.executeQuery();
		String definition, solution;
		int horizontal, ligne, colonne;
		while (rs2.next()) {
			definition = rs2.getString("definition");
			horizontal = rs2.getInt("horizontal");
			ligne = rs2.getInt("ligne");
			colonne = rs2.getInt("colonne");
			solution = rs2.getString("solution");

			int compteur = 0;

			if (horizontal == 1) {
				jeu.setDefinition(ligne, colonne, true, definition);
				for (int j = colonne; j <= colonne + solution.length() - 1; j++) {
					jeu.setCaseNoire(ligne, j, false);
					jeu.setSolution(ligne, j, solution.charAt(compteur++));
				}
			} else {
				jeu.setDefinition(ligne, colonne, false, definition);
				for (int i = ligne; i <= ligne + solution.length() - 1; i++) {
					jeu.setCaseNoire(i, colonne, false);
					jeu.setSolution(i, colonne, solution.charAt(compteur++));
				}
			}
		}

		return jeu;
	}

	public void addTextLimiter(final TextField tf, final int maxLength) {
		tf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(final ObservableValue<? extends String> ov, final String oldValue,
					final String newValue) {
				
				String s =  "";
				if (tf.getText().length() > maxLength) {
					s =  tf.getText().substring(newValue.length()-1, newValue.length());
					if(s.matches("[a-zA-Z]")) tf.setText(s);
					else tf.setText(oldValue);
				}else {
					if(!tf.getText().isEmpty())s= tf.getText().charAt(0)+"";
					if(s.matches("[a-zA-Z]")) tf.setText(s);
					else tf.setText("");
				}
				
				

				int lig = ((int) tf.getProperties().get("gridpane-row"))+1;
				int col = ((int) tf.getProperties().get("gridpane-column"))+1;
				
				
				if (checkAnswers &&!mc.jeu.getCellule(lig, col).isCaseNoire()
						&& mc.getProposition(lig, col) == mc.getSolution(lig, col)) {
					
					tf.setStyle(succesTextFieldStyle);
				}else {
					tf.setStyle(defaultTextFieldStyle);
				}
			}
		});
	}

}
