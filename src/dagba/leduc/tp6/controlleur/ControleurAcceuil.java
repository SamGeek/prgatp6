package dagba.leduc.tp6.controlleur;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ControleurAcceuil {
	
	public final String randomText ="Grille Aléatoire";	
	public final String chooseGridText ="Choisir la grille";	
	public final String exitText ="Quitter";
	
	@FXML
	private Pane containerPane;

	@FXML
	private Button grilleAleatoire;

	@FXML
	private Button choisirGrille;

	@FXML
	private ChoiceBox<String> choixNumGrille;

	@FXML
	private Button quitterJeu;

	// ------ Variables d'instance ----------

	static Map<Integer, String> grilles = new HashMap<Integer, String>();
	private static Connection maConnection;
	private ControleurTP6 controler;
	private int numGrilleToLoad = 0;
	private boolean isRandom = false;

	@FXML
	public void clicCase(MouseEvent e) {
		// changer later with mouse middle

		Stage stage = new Stage();
		Parent root;
		Button b = new Button();
		b = (Button) e.getSource();
		
		if (b.getText().equals(randomText)) {
			isRandom = true;
		}else if (b.getText().equals(chooseGridText)) {
			if(choixNumGrille.getSelectionModel().getSelectedItem()==null ) return;
			isRandom = false;
		}else if (b.getText().equals(exitText)) {
			System.exit(0);
		}
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../VueTP6.fxml")) ;
			controler = new ControleurTP6(isRandom, numGrilleToLoad);
			loader.setController(controler);
            root = (Parent) loader.load();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@FXML
	private void initialize() {
		
		try {
			maConnection = ControleurTP6.connecterBD();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		grillesDisponibles();
		
		for (Map.Entry<Integer, String> entry : grilles.entrySet()) {
			choixNumGrille.getItems().add(entry.getValue());
			choixNumGrille.setOnAction(a -> {
				 numGrilleToLoad = getKey(grilles, choixNumGrille.getValue());
			});
		}

		for (Node n : containerPane.getChildren()) {
			if (n instanceof Button) {
				Button btn = (Button) n;
				btn.setOnMouseClicked((e) -> {
					this.clicCase(e);
				});

			}
		}
	}


	public Map<Integer, String> grillesDisponibles() {
		String req1 = "SELECT num_grille, nom_grille, largeur, hauteur FROM TP5_GRILLE";
		PreparedStatement pstmt;
		try {
			pstmt = maConnection.prepareStatement(req1);
			ResultSet rs = pstmt.executeQuery(req1);
			String desc = "";
			int numero ,largeur,hauteur;
			String nom;
			while (rs.next()) {
				numero = rs.getInt("num_grille");
				nom = rs.getString("nom_grille");
				largeur = rs.getInt("largeur");
				hauteur = rs.getInt("hauteur");
				desc = nom + " (" + hauteur + "x" + largeur + ")";
				grilles.put(numero, desc);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return grilles;
	}

	public static <K, V> K getKey(Map<K, V> map, V value) {
		return map.entrySet().stream().filter(entry -> value.equals(entry.getValue())).map(Map.Entry::getKey)
				.findFirst().get();
	}

}
