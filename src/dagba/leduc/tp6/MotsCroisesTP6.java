package dagba.leduc.tp6;

import dagba.leduc.tp6.modele.Case;
import dagba.leduc.tp6.modele.Grille;
import javafx.beans.property.StringProperty;

public class MotsCroisesTP6 {

	// Variables d�instance
	public Grille<Case> jeu;

	public MotsCroisesTP6(int hauteur, int largeur) {

		jeu = new Grille<Case>(hauteur, largeur);
		for (int i = 1; i <= hauteur; i++) {
			for (int j = 1; j <= largeur; j++) {
				jeu.setCellule(i, j, new Case());
			}
		}
	}

	// Nombre de rang�es
	public int getHauteur() {
		return jeu.getHauteur();
	}

	// Nombre de colonnes
	public int getLargeur() {
		return jeu.getLargeur();
	}

	// Validit� des coordonn�es
	// Resultat : vrai si et seulement si (lig, col)
	// d�signent une cellule existante de la grille
	public boolean coordCorrectes(int lig, int col) {
		return jeu.coordCorrectes(lig, col);
	}

	// Accesseurs aux cases noires
	// Pr�condition (assert) : coordCorrectes(lig, col)
	public boolean estCaseNoire(int lig, int col) {
		assert jeu.coordCorrectes(lig, col) : "coordonnées incorrectes";
		return jeu.getCellule(lig, col).isCaseNoire();
	}

	public void setCaseNoire(int lig, int col, boolean noire) {

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		Case c = jeu.getCellule(lig, col);

		if (noire) {
			c.setCaseNoire(true);
			jeu.setCellule(lig, col, c);
		} else {
			c.setCaseNoire(false);
			c.setSolution(' ');
			jeu.setCellule(lig, col, c);
		}

	}

	// Accesseurs � la grille de solution
	// Pr�conditions (assert) : coordCorrectes(lig, col) !estCaseNoire(lig, col)
	public char getSolution(int lig, int col) {
		char sol = ' ';

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		if (!estCaseNoire(lig, col)) {
			sol = jeu.getCellule(lig, col).getSolution();
		}
		return sol;
	}

	public void setSolution(int lig, int col, char sol) {

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		Case c = new Case();
		c = (Case) jeu.getCellule(lig, col);

		c.setSolution(sol);
		jeu.setCellule(lig, col, c);

	}

	// Accesseurs � la grille du joueur
	// Pr�conditions : idem
	public char getProposition(int lig, int col) {

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		char prop = ' ';

		if (!estCaseNoire(lig, col) &&  !jeu.getCellule(lig, col).getProposition().get().isEmpty()) {
			prop = jeu.getCellule(lig, col).getProposition().get().charAt(0);
		}

		return prop;
	}

	public void setProposition(int lig, int col, char prop) {

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		Case c = jeu.getCellule(lig, col);
		c.setProposition(String.valueOf(prop));
		;

		jeu.setCellule(lig, col, c);

	}

	// Accesseurs aux d�finitions.
	// Le param�tre "horiz" est "true" pour les d�finitions
	// horizontales, "false" pour les d�finitions verticales.
	// Pr�conditions : idem
	public String getDefinition(int lig, int col, boolean horiz) {
		String def = "";

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		Case c = jeu.getCellule(lig, col);

		if (horiz && !estCaseNoire(lig, col)) {
			def = c.getHorizontal();
		} else if (!estCaseNoire(lig, col)) {
			def = c.getVertical();
		}
		return def;
	}

	public void setDefinition(int lig, int col, boolean horiz, String def) {

		assert coordCorrectes(lig, col) : "coordonnées incorrectes";

		Case c = jeu.getCellule(lig, col);

		if (horiz) {
			c.setHorizontal(def);
			jeu.setCellule(lig, col, c);
		} else {
			c.setVertical(def);
			jeu.setCellule(lig, col, c);
		}
	}
	
	public StringProperty propositionProperty(int lig, int col){
		 return jeu.getCellule(lig, col).getProposition();
	 }
	
	public void reveler(int lig, int col) {
		Case caseAReveler = jeu.getCellule(lig, col);
		setProposition(lig, col, caseAReveler.getSolution());
	}

	public String solutions() {
		IterateurMots it;
		String solution = "", next = "";
		for (int i = 0; i < jeu.getHauteur(); i++) {
			it = jeu.iterateurMots(true, i);

			while (it.hasNext()) {
				next = it.next();
				solution += it.next();
			}
		}

		return solution;
	}
}
